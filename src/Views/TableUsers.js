// @flow weak

import React from 'react';
import Svg from '../Svg';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';

const body = data => data.map((el, i) => (
  <tr key={i}>
    <td className="col01">
      <Link
        to={`users/${el.id}`} 
        className="user"
      >
        <Avatar 
          style={{marginRight: 10}}
          size={30}
          src={el.image_url}
          default={Svg.AvatarDefaultUser}
        />
        <div className="name">
        { `${el.first_name} ${el.last_name} ` }<span>{`(${el.nickname})`}</span>
        </div>
      </Link>
    </td>
    <td className="col02">
      { el.email }
    </td>
    <td className="col03">
      { el.hazelnuts_count }
    </td>
  </tr>
));

const pagination = () => (
  <div className="pagination">
    <button className="btn btn-primary">Prev</button>
    <button className="btn btn-primary">Next</button>
  </div>
);

const header = () => (
  <tr>
    <th className="col01">
      USER
    </th>
    <th className="col02">
      EMAIL
    </th>
    <th className="col03">
      HAZELNUTS
    </th>
  </tr>
);

const TableUsers = ({ data, icon, pagination: pag }) => data.length ? (
  <div>
    <table>
      <thead>
        { header() }
      </thead>
      <tbody>
        { body(data) }
      </tbody>
    </table>
    { pag ? pagination() : null}
  </div>
) : null;

export default TableUsers;
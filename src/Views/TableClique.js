// @flow weak

import React from 'react';
import Svg from '../Svg';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';

const body = data => data.map((el, i) => (
  <tr key={i}>
    <td className="col01">
      <Link
        to={`clique/${el.id}`} 
        className="user"
      >
        <Avatar 
          style={{marginRight: 10}}
          size={30}
          src={el.image_url}
          default={Svg.AvatarDefaultClique}
        />
        <div className="name">
        { el.name }
        </div>
      </Link>
    </td>
  </tr>
));

const pagination = () => (
  <div className="pagination">
    <button className="btn btn-primary">Prev</button>
    <button className="btn btn-primary">Next</button>
  </div>
);

const TableClique = ({ data, icon, pagination: pag }) => data.length ? (
  <div>
    <table>
      <thead>
        <tr>
          <th className="col01">
            CLIQUE
          </th>
        </tr>
      </thead>
      <tbody>
        { body(data) }
      </tbody>
    </table>
    { pag ? pagination() : null}
  </div>
) : null;

export default TableClique;
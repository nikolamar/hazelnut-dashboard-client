// @flow weak

import React from 'react';
import Navigation from '../Views/Navigation';
import Emitter from '../Lib/Emitter';

const deleteToken = () => {
  try {
    window.sessionStorage.removeItem("hazelnut-token");
    Emitter.emit("hazelnut-token", false);
  } catch (e) {
    console.error(e);
  }
};

const NavigationRoute = () => (
  <Navigation
    logo={
      <a 
        className="current logo">
        Hazelnut<br />
        <span>Admin</span>
      </a>
    }
    bottom={
      <Navigation.Link
        to="/login"
        icon="btnLogout"
        text="Logout"
        onTouch={deleteToken}
      />
    }
  >
    <Navigation.Link
      to="/users"
      icon="navUsers" 
      text="Users"
    />
    <Navigation.Link
      to="/clique"
      icon="navClique" 
      text="Clique"
    />
    <Navigation.Link
      to="/challenges"
      icon="navChallenges" 
      text="Challenges"
    />
  </Navigation>
);

export default NavigationRoute; 
// @flow weak

import React from 'react';
import { Link, Route } from 'react-router-dom';

const ClassicLink = ({ exact, icon, text, to, element, onTouch }) => (
  <Route 
    path={to} 
    exact={exact} 
    children={({ match }) => (
      !element ?
      <li
        className={`${match ? "current " : ""}${icon}`}
      >
        <Link 
          to={to}
          onClick={onTouch}
        >
          { text }
        </Link>
      </li> : element
    )}
  />
);

export default ClassicLink;
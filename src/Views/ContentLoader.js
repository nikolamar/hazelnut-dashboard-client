// @flow weak

import React from 'react';
import Svg from '../Svg';

const ContentLoader = ({ children, loading, animation: type, duration, style, className }) => (
  <div>
    <div 
      style={{
        animation: `${type} ${duration}s`, 
        ...style,
        display: loading ? "none" : "block",
      }} 
      className={className}
    >
      { children }
    </div>
    <div style={{
      position: "relative",
      height: "90vh",
      display: !loading ? "none" : "block"
    }}>
      <img
        alt="loader"
        width={70}
        height={70}
        style={{
          width: 70,
          height: 70,
          position: "absolute",
          top:0,
          bottom: 0,
          left: 0,
          right: 0,
          margin: "auto",
          animation: "fadeIn 0.2s",
        }}
        src={Svg.LoadingOrange}
      />
    </div>
  </div>
);

export default ContentLoader;
// @flow weak

import React, { Component } from 'react';
import Svg from '../Svg';
import Animation from '../Lib/Animation';

class LazyLoader extends Component {
  animation = new Animation();
  windowHeight = 0;
  onscroll = () => {
    this.windowHeight = Math.max(
      window.document.body.scrollHeight, window.document.documentElement.scrollHeight,
      window.document.body.offsetHeight, window.document.documentElement.offsetHeight,
      window.document.body.clientHeight, window.document.documentElement.clientHeight
    );
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    const currentBottom = window.innerHeight + scrollTop;
    if (currentBottom >= this.windowHeight && !this.props.loading) {
      this.onBottom();
    }
  }
  onBottom() {
    this.props.onBottom();
    this.animation.start(t => {
      this.refs.loader.width = this.props.loaderSize * t;
      this.refs.loader.height = this.props.loaderSize * t;
      this.refs.loader.style.width = `${this.props.loaderSize * t}px`;
      this.refs.loader.style.height = `${this.props.loaderSize * t}px`;
      this.refs.message.style["font-size"] = `${this.props.fontSize * t}px`;
    });
  }
  componentDidMount() {
    window.addEventListener("scroll", this.onscroll);
  }
  componentWillUnmount() {
    this.animation.cancel();
    window.removeEventListener("scroll", this.onscroll);
  }
  render() {
    return (
      <div ref="lazyLoader">
        { this.props.children }
        <img
          ref="loader"
          alt="loader"
          width={0}
          height={0}
          style={{
            margin: "auto",
            width: 0,
            height: 0,
            display: !this.props.loading ? "none" : "block"
          }}
          src={Svg.LoadingOrange}
        />
        <div
          ref="message"
          style={{ 
            margin: "auto",
            fontSize: 0,
            animation: "fadeIn 3s", 
            textAlign: "center",
            display: !this.props.loading ? "none" : "block"
          }}
        >
          {this.props.loadingMessage}
        </div>
      </div>
    );
  }
}

export default LazyLoader;

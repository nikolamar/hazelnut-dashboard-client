// @flow weak

import React from 'react';
import { Route } from 'react-router-dom';

const MyRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    <Component {...rest} {...props} />
  )}/>
);

export default MyRoute;
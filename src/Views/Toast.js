// @flow weak

import React from 'react';
import { toast as notify } from 'react-toastify';
import CONST from '../Lib/CONST';

const style = {
  message: {
    textAlign: "center",
    margin: 10,
  }
}

const Toast = ({ message }) => (
  <p style={style.message}>{message}</p>
);

export const toast = (message, type) => notify(
  <Toast message={message} />, 
  { type: type === CONST.TYPE.ERROR ? notify.TYPE.ERROR : type === CONST.TYPE.SUCCESS ? notify.TYPE.SUCCESS : notify.TYPE.INFO }
);

export default Toast;
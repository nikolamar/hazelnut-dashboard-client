// @flow weak

import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const MyRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => rest.token ? (
      <Component {...rest} {...props} />
    ) : (
      <Redirect to="/login" />
    )
  }/>
);

export default MyRoute;
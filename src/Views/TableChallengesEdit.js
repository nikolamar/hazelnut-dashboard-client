// @flow weak

import React from 'react';
import Svg from '../Svg';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';

const body = (data, remove, makeOwner, makeWinner) => data.map((el, i) => el.user ? (
  <tr key={i}>
    <td className="col01">
      <Link
        to={`/users/${el.user.id}`} 
        className="user"
      >
        <Avatar 
          style={{marginRight: 10}}
          size={30}
          src={el.user.image_url}
          default={Svg.AvatarDefaultUser}
        />
        <div className="name">
        { `${el.user.first_name} ${el.user.last_name} ` }<span>{`(${el.user.nickname})`}</span>
        </div>
      </Link>
    </td>
    <td className="col02">
      { el.user.email }
    </td>
    <td className="col03">
      { el.winner ? 
        <div 
          className="trophy"
        /> : 
        <button 
          type="button" 
          name="button" 
          className="btn btn-secondary btn-sm"
          onClick={e => makeWinner(e, el.user.id)}
        >
          Make Winner
        </button> }
    </td>
    <td className="col04">
      { el.owner ? 
        "Owner" : 
        <button 
          type="button" 
          name="button" 
          className="btn btn-secondary btn-sm"
          onClick={e => makeOwner(e, el.user.id)}
        >
          Make Owner
        </button> }
    </td>
    <td>
      <button
        className="btnRemove"
        icon={Svg.IconRemove}
        onClick={e => remove(e, el.user.id)}
      />
    </td>
  </tr>
) : null);

const TableChallengesEdit = ({ data, icon, remove, makeOwner, makeWinner }) => data.length && !Array.isArray(data[0]) ? (
  <div>
    <table>
      <thead>
        <tr>
          <th className="col01">
            USER
          </th>
          <th className="col02">
            EMAIL
          </th>
          <th className="col03">
            WINNER
          </th>
          <th className="col03">
            OWNER
          </th>
          <th className="colActions">
          </th>
        </tr>
      </thead>
      <tbody>
        { body(data, remove, makeOwner, makeWinner) }
      </tbody>
    </table>
  </div>
) : null;

export default TableChallengesEdit;
// @flow weak

import React from 'react';

const Content = ({ children, style }) => (
  <div 
    style={style} 
    className="mainContainer"
  >
    <div className="whiteContainer">
      { children }
    </div>
  </div>
);

export const Head = ({ children }) => (
  <div className="pageHead">
    { children }
  </div>
);

export const HeadLeft = ({ children }) => (
  <div className="pageHeadLeft">
    { children }
  </div>
);

export const HeadRight = ({ children }) => (
  <div className="pageHeadRight">
    { children }
  </div>
);

export const SubHead = ({ children }) => (
  <div className="pageSubhead">
    { children }
  </div>
);

export const SubHeadLeft = ({ children }) => (
  <div className="pageSubheadLeft">
    { children }
  </div>
);

export const SubHeadRight = ({ children }) => (
  <div className="pageSubheadRight">
    { children }
  </div>
);

export const Padd = ({ padd, children }) => (
  <div className={padd ? `paddingSide${padd}` : null}>
    { children }
  </div>
);

export const Row = ({ children }) => (
  <div className="row">
    { children }
  </div>
);

export const Col = ({ children, col }) => (
  <div className={`col${col ? `-${col}` : ""}`}>
    { children }
  </div>
);

export default Content;
// @flow weak

import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import NavigationRoute from './Views/NavigationRoute';
import PublicRoute from './Views/PublicRoute';
import ProtectedRoute from './Views/ProtectedRoute';
import Login from './Screens/Login';
import Users from './Screens/Users';
import UserEdit from './Screens/UserEdit';
import Clique from './Screens/Clique';
import CliqueEdit from './Screens/CliqueEdit';
import Challenges from './Screens/Challenges';
import ChallengesEdit from './Screens/ChallengesEdit';
import NoMatch from './Screens/NoMatch';
import Emitter from './Lib/Emitter';
import Animate from './Views/Animate';
import 'react-toastify/dist/ReactToastify.min.css'; 
import './Styles/Animate.css';
import './Styles/App.css';

class App extends Component {
  state = {
    token: "",
  }
  subscription = {};
  componentWillMount() {
    const token = window.sessionStorage.getItem("hazelnut-token");
    this.setState({ token });
  }
  componentDidMount() {
    // Emmits from Login Component / handleSubmit function
    this.subscription = Emitter.addListener("hazelnut-token", token => this.setState({ token }));
  }
  componentWillUnmount() {
    this.subscription.remove();
  }
  render() {
    const { token } = this.state;
    return (
      <Router>
        <Animate animation="fadeIn" duration={1}>
          <Route
            exact path="/"
            render={() => <Redirect to="/users" />}
          />
          <ProtectedRoute
            token={token}
            path="/" 
            component={NavigationRoute}
          />
          <Switch>
            <Route 
              path="/users" 
              render={({ match }) => (
                <div>
                  <Switch>
                    <ProtectedRoute
                      token={token}
                      exact path={match.url}
                      component={Users} 
                    />
                    <PublicRoute
                      path={`${match.url}/:id`} 
                      component={UserEdit} 
                    />
                    <Route 
                      component={NoMatch}
                    />
                  </Switch>
                </div>
              )}
            />
            <Route 
              path="/clique" 
              render={({ match }) => (
                <div>
                  <Switch>
                    <ProtectedRoute
                      token={token}
                      exact path={match.url}
                      component={Clique} 
                    />
                    <PublicRoute 
                      path={`${match.url}/:id`} 
                      component={CliqueEdit} 
                    />
                    <Route 
                      component={NoMatch}
                    />
                  </Switch>
                </div>
              )}
            />
            <Route 
              path="/challenges" 
              render={({ match }) => (
                <div>
                  <Switch>
                    <ProtectedRoute
                      token={token}
                      exact path={match.url}
                      component={Challenges} 
                    />
                    <PublicRoute 
                      path={`${match.url}/:id`} 
                      component={ChallengesEdit} 
                    />
                    <Route 
                      component={NoMatch}
                    />
                  </Switch>
                </div>
              )}
            />
            <PublicRoute
              token={token}
              exact path="/login" 
              component={Login}
            />
            <Route 
              component={NoMatch}
            />
          </Switch>
          <ToastContainer autoClose={3000} position="bottom-center"/>
        </Animate>
      </Router>
    );
  }
}

export default App;

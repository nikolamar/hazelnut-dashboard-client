// @flow weak

import axios from "axios";
import CONST from "./CONST";
import runtimeEnv from '@mars/heroku-js-runtime-env';

const CancelToken = axios.CancelToken;

const env = runtimeEnv();
// const baseURL = CONST.BASE_URL;
// const API_KEY = CONST.API_KEY;
// const API_SECRET = CONST.API_SECRET;
// const CLOUDINARY_CLOUD_NAME = CONST.CLOUDINARY_CLOUD_NAME;
// const CLOUDINARY_PRESET_NAME = CONST.CLOUDINARY_PRESET_NAME;

const baseURL = env.REACT_APP_BASE_URL;
const API_KEY = env.REACT_APP_API_KEY;
const API_SECRET = env.REACT_APP_API_SECRET;
const CLOUDINARY_CLOUD_NAME = env.REACT_APP_CLOUDINARY_CLOUD_NAME;
const CLOUDINARY_PRESET_NAME = env.REACT_APP_CLOUDINARY_PRESET_NAME;

class Api {
  static axios = axios;
  static cancelToken = {};
  static config = () => ({
    baseURL,
    headers: {
      "Authorization": `Bearer ${JSON.parse(window.sessionStorage.getItem("hazelnut-token"))}`,
      "Content-Type": "application/json; charset=utf-8",
    },
  });
  static cancel() {
    try {
      this.cancelToken.cancel('Operation canceled by the user.');
    }
    catch(e) {
      console.warn("There's not token to cancel.");
    }
    return this;
  }
  static get(url) {
    this.cancelToken = CancelToken.source();
    return axios({
      ...this.config(),
      url, 
      method: "GET",
      cancelToken: this.cancelToken.token,
    });
  }
  static put(url, data) {
    this.cancelToken = CancelToken.source();
    return axios({
      ...this.config(),
      url,
      method: "PUT",
      data: JSON.stringify(data),
    });
  }
  static patch(url, data) {
    this.cancelToken = CancelToken.source();
    return axios({
      ...this.config(),
      url,
      method: "PATCH",
      data: JSON.stringify(data),
    });
  }
  static post(url, data) {
    this.cancelToken = CancelToken.source();
    return axios({
      ...this.config(),
      url,
      method: "POST",
      data: JSON.stringify(data),
    });
  }
  static delete(url) {
    this.cancelToken = CancelToken.source();
    return axios({
      ...this.config(),
      url, 
      method: "DELETE",
    });
  }

  // UTILS
  static cloudinaryUpload(file) {
    let data = new FormData();
    data.append("file", file);
    data.append("upload_preset", CLOUDINARY_PRESET_NAME);
    const config = {
      method: "POST",
      headers: {
        "Content-Type": "multipart/form-data;",
      },
      baseURL: "https://api.cloudinary.com/api/",
      url: `/v1_1/${CLOUDINARY_CLOUD_NAME}/image/upload`,
      data,
    }
    return axios(config);
  }

  // API SPECIFICATION

  // GET
  static getChallenge(id) {
    return this.get(`/api/v1/admin/challenges/${id}`);
  }
  static getChallenges(page, size) {
    let params = "";
    if (page && size) params = `?page=${page}&size=${size}`;
    return this.get(`/api/v1/admin/challenges${params}`);
  }
  static getCliques(page, size) {
    let params = "";
    if (page && size) params = `?page=${page}&size=${size}`;
    return this.get(`/api/v1/admin/cliques${params}`);
  }
  static searchCliques(query) {
    return this.get(`/api/v1/admin/search/cliques?q=${query}`);
  }
  static getCliqueDetails(id) {
    return this.get(`/api/v1/admin/cliques/${id}`);
  }
  static getUser(id) {
    return this.get(`/api/v1/admin/users/${id}`);
  }
  static getUsers(page, size) {
    let params = "";
    if (page && size) params = `?page=${page}&size=${size}`;
    return this.get(`/api/v1/admin/users${params}`);
  }
  static searchUsers(query) {
    return this.get(`/api/v1/admin/search/users?q=${query}`);
  }
  static searchChallenges(query) {
    return this.get(`/api/v1/admin/search/challenges?q=${query}`);
  }

  // POST
  static basicAuthLogin(username, password) {
    const config = {
      method: "POST",
      headers: {
        "Authorization": `Basic ${window.btoa(`${API_KEY}:${API_SECRET}`)}`,
        "Content-Type": "application/json; charset=utf-8",
      },
      baseURL,
      url: "/api/v1/admin/login",
      data: {
        username,
        password,
      },
    };
    return axios(config);
  }
  static createUser(data) {
    return this.post(`/api/v1/admin/users`, data);
  }
  static createChallenge(data) {
    return this.post(`/api/v1/admin/challenges`, data);
  }
  static createClique(data) {
    return this.post(`/api/v1/admin/cliques`, data);
  }

  // PUT
  static updateOwnerForChallenge(challengeId, userId) {
    return this.put(`/api/v1/admin/challenges/${challengeId}/owner/${userId}`);
  }
  static updateWinnerForChallenge(challengeId, userId) {
    return this.put(`/api/v1/admin/challenges/${challengeId}/winner/${userId}`);
  }
  static addUserToChallenge(challengeId, userId) {
    return this.put(`/api/v1/admin/challenges/${challengeId}/users/${userId}`);
  }
  static addMemberToClique(cliqueId, userId, data) {
    return this.put(`/api/v1/admin/cliques/${cliqueId}/members/${userId}`, data);
  }

  // PATCH
  static updateClique(data, id) {
    return this.patch(`/api/v1/admin/cliques/${id}`, data);
  }
  static updateUser(data, id) {
    return this.patch(`/api/v1/admin/users/${id}`, data);
  }
  static updateChallenge(data, id) {
    return this.patch(`/api/v1/admin/challenges/${id}`, data);
  }

  // DELETE
  static deleteChallenge(id) {
    return this.delete(`/api/v1/admin/challenges/${id}`);
  }
  static deleteClique(id) {
    return this.delete(`/api/v1/admin/cliques/${id}`);
  }
  static deleteUser(id) {
    return this.delete(`/api/v1/admin/users/${id}`);
  }
  static removeUserFromChallenge(challengeId, userId) {
    return this.delete(`/api/v1/admin/challenges/${challengeId}/users/${userId}`);
  }
  static removeMemberFromClique(cliqueId, memberId) {
    return this.delete(`/api/v1/admin/cliques/${cliqueId}/members/${memberId}`);
  }
};

export default Api;
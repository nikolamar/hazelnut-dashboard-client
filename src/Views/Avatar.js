// @flow weak

import React, { Component } from 'react';
import Svg from '../Svg';

const style = {
  container: {
    position: "relative",
    overflow: "hidden",
  },
  layer: {
    position: "absolute",
    animation: "fadeIn 2s",
    top: 0, left: 0, bottom: 0, right: 0,
  }
}

class Avatar extends Component {
  state = {
    isLoading: true,
  }
  onLoad = () => {
    this.setState({ isLoading: false });
  }
  onError = () => {
    this.refs.avatar.src = this.props.default;
  }
  render() {
    return (
      <div 
        style={{
          ...style.container,
          ...this.props.style,
          width: this.props.size,
          height: this.props.size,
          display: "inline-block",
          verticalAlign: "middle",
          marginRight: 20,
        }}
      >
        <img
          alt="back"
          style={{
            ...style.layer,
            borderRadius: this.props.size,
            display: this.state.isLoading ? "block" : "none"
          }} 
          src={this.props.default}
          width={this.props.size} 
          height={this.props.size}
        />
        <img
          alt="avatar"
          ref="avatar"
          style={{
            ...style.layer, 
            borderRadius: this.props.size, 
            display: this.state.isLoading ? "none" : "block"
          }}
          src={this.props.src || this.props.default}
          onLoad={this.onLoad}
          onError={this.onError}
          width={this.props.size} 
          height={this.props.size}
        />
        <img
          alt="loader"
          style={{
            ...style.layer,
            borderRadius: this.props.size,
            display: this.state.isLoading ? "block" : "none"
          }} 
          src={Svg.LoadingOrange}
          width={this.props.size} 
          height={this.props.size}
        />
      </div>
    );
  }
};

export default Avatar;
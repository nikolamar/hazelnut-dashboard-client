// @flow weak

import React from 'react';

const Animate = ({ children, animation, duration, style, className }) => (
  <div 
    style={{
      animation: `${animation} ${duration}s`,
      ...style,
    }} 
    className={className}
  >
    { children }
  </div>
);

export default Animate;
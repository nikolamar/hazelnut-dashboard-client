import AvatarDefaultChallenge from './avatar-default-challenge.svg';
import AvatarDefaultClique from './avatar-default-clique.svg';
import AvatarDefaultUser from './avatar-default-user.svg';
import IconChallenges from './icon-challenges.svg';
import IconClique from './icon-clique.svg';
import IconLogout from './icon-logout.svg';
import IconRemove from './icon-search.svg';
import IconTrash from './icon-trash.svg';
import IconTrophy from './icon-trophy.svg';
import IconUsers from './icon-users.svg';
import Logo from './logo.svg';
import LoadingWhite from './loading-white.svg';
import LoadingBlack from './loading-black.svg';
import LoadingOrange from './loading-orange.svg';

const Svg = {
  AvatarDefaultChallenge,
  AvatarDefaultClique,
  AvatarDefaultUser,
  IconChallenges,
  IconClique,
  IconLogout,
  IconRemove,
  IconTrash,
  IconTrophy,
  IconUsers,
  Logo,
  LoadingWhite,
  LoadingBlack,
  LoadingOrange,
};

export default Svg;
// @flow weak

import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import Content, {
  Head,
  HeadLeft, 
  HeadRight,
  Padd,
  Row,
  Col, 
} from '../Views/Content';
import Modal from '../Views/Modal';
import Api from '../Lib/Api';
import CONST from '../Lib/CONST';
import Avatar from '../Views/Avatar';
import { toast } from '../Views/Toast';
import LoadingButton from '../Views/LoadingButton';
import ContentLoader from '../Views/ContentLoader';
import Svg from '../Svg';
import KEYCODE from '../Lib/KEYCODE';

class UserEdit extends Component {
  state = {
    modalPhoto: false,
    modalUser: false,
    isPageLoading: true,
    isPhotoLoading: false,
    isUserLoading: false,
    id: null,
    first_name: "",
    last_name: "",
    email: "",
    nickname: "",
    password: "",
    date_of_birth: "",
    gender: "",
    image_url: "",
    hazelnuts_count: 0,
    status_hazelnuts_count: 0,
    place_id: "",
    hometown: "",
  };
  hometownSearch = {};
  keydownListener = {};
  placeChangedListener = {};
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  };
  handleChoosePhoto = () => {
    this.refs.upload.click();
  };
  handleAvatar = event => {
    const file = event.target.files[0];
    this.setState({ isPhotoLoading: true });
    Api
      .cloudinaryUpload(file)
      .then(({ data }) => {
        this.setState({
          image_url: data.secure_url,
          isPhotoLoading: false,
        });
        this.refs.upload.value = null;
      })
      .catch(error => {
        this.refs.upload.value = null;
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isUserLoading: true });
    let data = {};
    if (this.state.place_id) data.place_id = this.state.place_id;
    data.image_url = this.state.image_url;
    data.date_of_birth = this.state.date_of_birth;
    data.gender = this.state.gender;
    data.nickname = this.state.nickname;
    data.email = this.state.email;
    data.hazelnuts_count = this.state.hazelnuts_count;
    data.status_hazelnuts_count = this.state.status_hazelnuts_count;
    data.first_name =  this.state.first_name;
    data.last_name = this.state.last_name;
    (this.state.id ? Api.updateUser(data, this.state.id) : Api.createUser(data, this.state.id))
      .then(({ data }) => {
        this.state.id ?
        toast(`${CONST.MESSAGE.UPDATED} ${data.nickname}!`, CONST.TYPE.SUCCESS) :
        toast(`${CONST.MESSAGE.ADDED} ${data.nickname}!`, CONST.TYPE.SUCCESS);
        const dateOfBirthFormated = moment(data.date_of_birth).format("YYYY-MM-DD");
        this.setState({
          isUserLoading: false,
          ...data,
          date_of_birth: dateOfBirthFormated,
          hometown: data.location ? data.location.name : "",
        });
        this.props.history.push(`/users/${data.id}`);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isUserLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleRemovePhoto = () => {
    this.setState({ 
      isPhotoLoading: true,
      modalPhoto: false,
    });
    Api
      .updateUser({ image_url: "" }, this.state.id)
      .then(({ data }) => {
        toast(`${CONST.MESSAGE.PHOTO_REMOVED} ${data.nickname}!`, CONST.TYPE.SUCCESS);
        this.setState({
          isPhotoLoading: false,
          image_url: "",
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleDelete = () => {
    this.setState({ 
      isUserLoading: true,
      modalUser: false,
    });
    Api
      .deleteUser(this.state.id)
      .then(() => {
        this.props.history.push("/users");
        toast(`${CONST.MESSAGE.DELETED} ${this.state.nickname}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isUserLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  refresh = () => {
    Api
      .getUser(this.props.match.params.id)
      .then(({ data }) => {
        const dateOfBirthFormated = moment(data.date_of_birth).format("YYYY-MM-DD");
        this.setState({
          isPageLoading: false,
          isUserLoading: false,
          ...data,
          date_of_birth: dateOfBirthFormated,
          hometown: data.location ? data.location.name : "",
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ 
            isPageLoading: false,
            isUserLoading: false,
          });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  componentDidMount() {
    if (!window.google) return;
    this.hometownSearch = new window.google.maps.places.Autocomplete(
      this.refs.hometown, { types: ["(cities)"]}
    );
    this.keydownListener = window.google.maps.event.addDomListener(this.refs.hometown, "keydown", event => { 
      if (event.keyCode === KEYCODE.ENTER) {
        event.preventDefault(); 
      }
    });
    this.placeChangedListener = this.hometownSearch.addListener("place_changed", () => {
      const place = this.hometownSearch.getPlace();
      this.setState({
        place_id: place.place_id,
        hometown: place.name,
      });
    });
  }
  componentWillMount = () => {
    if (isNaN(this.props.match.params.id)) {
      this.setState({ isPageLoading: false });
      return;
    }
    this.refresh();
  };
  componentWillUnmount() {
    window.google.maps.event.clearInstanceListeners(this.keydownListener);
    window.google.maps.event.clearInstanceListeners(this.placeChangedListener);
    this.hometownSearch = null;
    Api.cancel();
  }
  render() {
    return (
      <Content>
        <ContentLoader 
          animation="fadeIn" 
          duration={0.5}
          loading={this.state.isPageLoading}
        >
          <Head>
            <HeadLeft>
              <h2>{`${this.state.first_name} ${this.state.last_name}`}
                <span>{this.state.id ? `ID: ${this.state.id}` : "ID:"}</span>
              </h2>
            </HeadLeft>
            <HeadRight>
              <button
                type="button"
                className="btn btnDelete"
                onClick={() => this.setState({ modalUser: true })}
              >
                Delete
              </button>
            </HeadRight>
          </Head>
          <Padd padd={30}>
            <Row>
              <Col>
                <form onSubmit={this.handleSubmit}>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="first_name"
                      className="col-4 col-form-label">
                      First Name:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="first_name"
                        className="form-control"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.first_name}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="last_name"
                      className="col-4 col-form-label"
                    >
                      Last Name:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="last_name"
                        className="form-control"
                        type="text"
                        onChange={this.handleChange} 
                        value={this.state.last_name}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label
                      htmlFor="email"
                      className="col-4 col-form-label">
                      Email:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="email"
                        required="true"
                        className="form-control"
                        type="email"
                        onChange={this.handleChange}
                        value={this.state.email}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="nickname"
                      className="col-4 col-form-label">
                      Nickname:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="nickname"
                        required="true"
                        className="form-control"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.nickname}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label
                      htmlFor="hometown"
                      className="col-4 col-form-label">
                      Hometown:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input 
                        id="hometown"
                        ref="hometown"
                        required="true"
                        placeholder=""
                        className="form-control"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.hometown}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="date_of_birth"
                      className="col-4 col-form-label">
                      Date of Birth:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="date_of_birth"
                        className="form-control"
                        type="date"
                        onChange={this.handleChange}
                        value={this.state.date_of_birth}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="gender" 
                      className="col-4 col-form-label">
                      Gender:
                    </label>
                    <div
                      className="col-8"
                    >
                      <select
                        id="gender"
                        className="form-control"
                        onChange={this.handleChange}
                        value={this.state.gender}
                      >
                        <option>Male</option>
                        <option>Female</option>
                      </select>
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="status_hazelnuts_count"
                      className="col-4 col-form-label">
                      Status Hazelnuts:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="status_hazelnuts_count"
                        className="form-control"
                        type="number"
                        onChange={this.handleChange}
                        value={this.state.status_hazelnuts_count}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="hazelnuts_count"
                      className="col-4 col-form-label">
                      Hazelnuts:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input 
                        id="hazelnuts_count"
                        className="form-control"
                        type="number"
                        onChange={this.handleChange}
                        value={this.state.hazelnuts_count}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <div
                      className="col-8 offset-4"
                    >
                      <LoadingButton
                        type="submit"
                        loading={this.state.isUserLoading}
                      >
                        Save
                      </LoadingButton>
                    </div>
                  </div>
                </form>
              </Col>
              <Col>
                <div
                  className="avatarUpload"
                >
                  <Avatar 
                    size={100}
                    src={this.state.image_url}
                    default={Svg.AvatarDefaultUser}
                  />
                  <LoadingButton
                    type="button"
                    loading={this.state.isPhotoLoading}
                    onClick={this.handleChoosePhoto}
                  >
                    Choose Photo
                  </LoadingButton>
                  <input
                    id="file"
                    ref="upload"
                    type="file"
                    style={{ display: "none" }}
                    onChange={this.handleAvatar}
                  />
                  <button
                    type="button"
                    className="linkRed"
                    onClick={() => this.setState({ modalPhoto: true })}
                  >
                    Remove
                  </button>
                </div>
              </Col>
            </Row>
          </Padd>
        </ContentLoader>
        <Modal
          visible={this.state.modalPhoto}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_PHOTO}
          cancel={() => this.setState({ modalPhoto: false })}
          yes={this.handleRemovePhoto}
        />
        <Modal
          visible={this.state.modalUser}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_USER}
          cancel={() => this.setState({ modalUser: false })}
          yes={this.handleDelete}
        />
      </Content>
    )
  }
}

export default withRouter(UserEdit);
// @flow weak

import React from 'react';
import Svg from '../Svg';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';

const body = (data, remove) => data.map((el, i) => el.member ? (
  <tr key={i}>
    <td className="col01">
      <Link
        to={`/users/${el.member.id}`} 
        className="user"
      >
        <Avatar 
          style={{marginRight: 10}}
          size={30}
          src={el.member.image_url}
          default={Svg.AvatarDefaultUser}
        />
        <div className="name">
        { `${el.member.first_name} ${el.member.last_name} ` }<span>{`(${el.member.nickname})`}</span>
        </div>
      </Link>
    </td>
    <td className="col02">
      { el.member.email }
    </td>
    <td className="col03">
      { el.member.hazelnuts_count }
    </td>
    <td>
      <button
        className="btnRemove"
        icon={Svg.IconRemove}
        onClick={e => remove(e, el.member.id)}
      />
    </td>
  </tr>
) : null);

const TableCliqueEdit = ({ data, icon, remove }) => data.length && !Array.isArray(data[0]) ? (
  <div>
    <table>
      <thead>
        <tr>
          <th className="col01">
            USER
          </th>
          <th className="col02">
            EMAIL
          </th>
          <th className="col03">
            HAZELNUTS
          </th>
          <th className="colActions">
          </th>
        </tr>
      </thead>
      <tbody>
        { body(data, remove) }
      </tbody>
    </table>
  </div>
) : null;

export default TableCliqueEdit;
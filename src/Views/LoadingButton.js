// @flow weak

import React from 'react';
import Svg from '../Svg';

const LoadingButton = ({ children, onClick, type, loading }) => (
  <button
    className="btn btn-primary"
    type={type}
    onClick={onClick}
  >
    { children }
    <img
      alt="search"
      style={{
        opacity: 0.5,
        width: loading ? 20 : 0,
        height: loading ? 20 : 0,
        marginLeft: loading ? 10 : 0,
        marginBottom: loading ? 2 : 0,
        marginRight: 0,
        transition: "all 0.15s"
      }} 
      src={Svg.LoadingWhite}
    />
  </button>
);

export default LoadingButton;
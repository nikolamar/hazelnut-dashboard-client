// @flow weak

const timeout = callback => window.setTimeout(callback, 1000 / 60);
const RequestAnimationFrame = window.requestAnimationFrame ||
window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || timeout;

export default RequestAnimationFrame;


// @flow weak

import React from 'react';
import moment from 'moment';
import Svg from '../Svg';
import { Link } from 'react-router-dom';
import Avatar from './Avatar';
import CONST from '../Lib/CONST';

const body = data => data.map((el, i) => (
  <tr key={i}>
    <td className="col01">
      <Link
        to={`challenges/${el.id}`} 
        className="user"
      >
        <Avatar 
          style={{marginRight: 10}}
          size={30}
          src={el.image_url}
          default={Svg.AvatarDefaultClique}
        />
        <div className="name">
          { el.discipline }
        </div>
      </Link>
    </td>
    <td className="col02">
      { CONST.DISCIPLINE[el.discipline_category_id] }
    </td>
    <td className="col03">
      { el.challenge_users.map(challenge => (challenge.user && challenge.owner) ? challenge.user.nickname : null) }
    </td>
    <td className="col04">
      { el.challenge_users.map(challenge => (challenge.user && !challenge.owner) ? challenge.user.nickname : null) }
    </td>
    <td className="col05">
      { moment(el.start_date).format("DD/MM/YYYY") }
    </td>
  </tr>
));

const pagination = () => (
  <div className="pagination">
    <button className="btn btn-primary">Prev</button>
    <button className="btn btn-primary">Next</button>
  </div>
);

const TableChallenges = ({ data, icon, pagination: pag }) => data.length ? (
  <div>
    <table>
      <thead>
        <tr>
          <th className="col01">
            DISCIPLINE
          </th>
          <th className="col02">
            CATEGORY
          </th>
          <th className="col03">
            CHALLENGER
          </th>
          <th className="col04">
            CHALLENGEE
          </th>
          <th className="col05" style={{ whiteSpace: "nowrap" }}>
            START DATE
          </th>
        </tr>
      </thead>
      <tbody>
        { body(data) }
      </tbody>
    </table>
    { pag ? pagination() : null}
  </div>
) : null;

export default TableChallenges;
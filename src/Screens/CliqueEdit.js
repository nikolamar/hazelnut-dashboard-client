// @flow weak

import React, { Component } from 'react';
import Content, {
  Head, 
  HeadLeft, 
  HeadRight,
  SubHead,
  SubHeadLeft,
  Padd,
  Row,
  Col, 
} from '../Views/Content';
import Modal from '../Views/Modal';
import Api from '../Lib/Api';
import CONST from '../Lib/CONST';
import { toast } from '../Views/Toast';
import LoadingButton from '../Views/LoadingButton';
import Avatar from '../Views/Avatar';
import ContentLoader from '../Views/ContentLoader';
import Autocomplete from '../Views/Autocomplete';
import Svg from '../Svg';
import TableCliqueEdit from '../Views/TableCliqueEdit';
import KEYCODE from '../Lib/KEYCODE';

class CliqueEdit extends Component {
  state = {
    modalPhoto: false,
    modalClique: false,
    modalMember: false,
    isPageLoading: true,
    isPhotoButtonLoading: false,
    isCreatorSearchLoading: false,
    isMemberButtonLoading: false,
    isMemberSearchLoading: false,
    isCliqueLoading: false,
    member_id_to_remove: null,
    creator: {},
    creatorQuery: "",
    creatorResult: [],
    member: {},
    memberQuery: "",
    memberResult: [],
    id: null,
    name: "",
    place_id: "",
    creator_id: null,
    image_url: "",
    hometown: "",
    memberships: [],
  }
  hometownSearch = {};
  keydownListener = {};
  placeChangedListener = {};
  handleChange = event => {
    switch(event.target.id) {
      case "creatorQuery":
        if (!event.target.value) {
          this.setState({
            creatorResult: [],
            isCreatorSearchLoading: false,
            [event.target.id]: event.target.value,
          });
          return;
        }
        this.setState({
          isCreatorSearchLoading: true,
          [event.target.id]: event.target.value,
        });
        Api
          .cancel()
          .searchUsers(event.target.value)
          .then(({ data }) => {
            this.setState({ 
              isCreatorSearchLoading: false,
              creatorResult: data 
            });
          })
          .catch(error => {
            if (Api.axios.isCancel(error)) {} else {
              this.setState({ isCreatorSearchLoading: false });
              toast((error.response && error.response.data.message) 
              || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
            }
          });
        break;
      case "memberQuery":
        if (!event.target.value) {
          this.setState({
            memberResult: [],
            isMemberSearchLoading: false,
            [event.target.id]: event.target.value,
          });
          return;
        }
        this.setState({
          isMemberSearchLoading: true,
          [event.target.id]: event.target.value,
        });
        Api
          .cancel()
          .searchUsers(event.target.value)
          .then(({ data }) => {
            this.setState({ 
              isMemberSearchLoading: false,
              memberResult: data 
            });
          })
          .catch(error => {
            if (Api.axios.isCancel(error)) {} else {
              this.setState({ isMemberSearchLoading: false });
              toast((error.response && error.response.data.message) 
              || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
            }
          });
        break;
      default:
        this.setState({ [event.target.id]: event.target.value });
        break;
    }
  };
  handleChoosePhoto = () => {
    this.refs.upload.click();
  };
  handleAvatar = event => {
    const file = event.target.files[0];
    this.setState({ isPhotoButtonLoading: true });
    Api
      .cloudinaryUpload(file)
      .then(({ data }) => {
        this.setState({
          image_url: data.secure_url,
          isPhotoButtonLoading: false,
        });
        this.refs.upload.value = null;
      })
      .catch(error => {
        this.refs.upload.value = null;
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isCliqueLoading: true });
    let data = {};
    if (this.state.place_id) data.place_id = this.state.place_id;
    data.name = this.state.name;
    data.image_url = this.state.image_url;
    data.creator_id = this.state.creator_id;
    (this.state.id ? Api.updateClique(data, this.state.id) : Api.createClique(data, this.state.id))
      .then(({ data }) => {
        this.state.id ?
        toast(`${CONST.MESSAGE.UPDATED} ${data.name}!`, CONST.TYPE.SUCCESS) :
        toast(`${CONST.MESSAGE.ADDED} ${data.name}!`, CONST.TYPE.SUCCESS);
        this.setState({
          isCliqueLoading: false,
          ...data,
        });
        this.props.history.push(`/clique/${data.id}`);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isCliqueLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleSelectCreator = (event, i) => {
    this.setState({
      creator: this.state.creatorResult[i],
      creator_id: this.state.creatorResult[i].id,
      creatorQuery: this.state.creatorResult[i].nickname
    });
  };
  handleSelectMember = (event, i) => {
    this.setState({
      member: this.state.memberResult[i],
      memberQuery: this.state.memberResult[i].nickname
    });
  };
  handleAddMember = event => {
    this.setState({ isMemberButtonLoading: true });
    let data = this.state.memberships.slice();
    if (this.state.memberQuery !== this.state.member.nickname)
      data.push({ 
        member: this.state.memberResult.filter(member => member.nickname === this.state.memberQuery)[0] 
      });
    else
      data.push({ 
        member: this.state.member 
      });
    if (!data[data.length-1].member) {
      toast(CONST.MESSAGE.MEMBER_DOES_NOT_EXIST, CONST.TYPE.ERROR);
      return;
    }
    Api
      .addMemberToClique(
        this.state.id, 
        data[data.length-1].member.id, 
        { "hazelnuts_count": "100" },
      )
      .then(() => {
        this.setState({ 
          isMemberButtonLoading: false,
          memberships: data,
        });
        toast(`${CONST.MESSAGE.MEMBER_ADDED} ${this.state.name}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isMemberButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleRemoveMember = () =>  {
    this.setState({ 
      isMemberButtonLoading: true,
      modalMember: false,
    });
    Api
      .removeMemberFromClique(this.state.id, this.state.member_id_to_remove)
      .then(() => {
        this.setState({ 
          isMemberButtonLoading: false,
          memberships: this.state.memberships.filter(
            ({ member }) => member.id !== this.state.member_id_to_remove),
        });
        toast(`${CONST.MESSAGE.MEMBER_REMOVED} ${this.state.name}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isMemberButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleRemovePhoto = () => {
    this.setState({ 
      isPhotoButtonLoading: true,
      modalPhoto: false,
    });
    Api
      .updateClique({ image_url: "" }, this.state.id)
      .then(({ data }) => {
        toast(`${CONST.MESSAGE.PHOTO_REMOVED} ${data.name}!`, CONST.TYPE.SUCCESS);
        this.setState({
          isPhotoButtonLoading: false,
          image_url: "",
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleDelete = () => {
    this.setState({ 
      isCliqueLoading: true,
      modalClique: false,
    });
    Api
      .deleteClique(this.state.id)
      .then(() => {
        this.props.history.push("/clique");
        toast(`${CONST.MESSAGE.DELETED} ${this.state.name}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isCliqueLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  refresh = () => {
    Api
      .getCliqueDetails(this.props.match.params.id)
      .then(({ data }) => {
        this.setState({
          isPageLoading: false,
          ...data,
          hometown: data.location ? data.location.name : "",
        });
        return Api.getUser(data.creator_id);
      })
      .then(({ data }) => {
        this.setState({ 
          isCliqueLoading: false,
          creatorQuery: data.nickname,
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ 
            isPageLoading: false,
            isCliqueLoading: false,
          });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  componentDidMount() {
    if (!window.google) return;
    this.hometownSearch = new window.google.maps.places.Autocomplete(
      this.refs.hometown, { types: ["(cities)"]}
    );
    this.keydownListener = window.google.maps.event.addDomListener(this.refs.hometown, "keydown", event => { 
      if (event.keyCode === KEYCODE.ENTER) {
        event.preventDefault(); 
      }
    });
    this.placeChangedListener = this.hometownSearch.addListener("place_changed", () => {
      const place = this.hometownSearch.getPlace();
      this.setState({
        place_id: place.place_id,
        hometown: place.name,
      });
    });
  }
  componentWillMount = () => {
    if (isNaN(this.props.match.params.id)) {
      this.setState({ isPageLoading: false });
      return;
    }
    this.refresh();
  };
  componentWillUnmount() {
    window.google.maps.event.clearInstanceListeners(this.keydownListener);
    window.google.maps.event.clearInstanceListeners(this.placeChangedListener);
    this.hometownSearch = null;
    Api.cancel();
  }
  render() {
    return (
      <Content>
        <ContentLoader 
          animation="fadeIn" 
          duration={0.5}
          loading={this.state.isPageLoading}
        >
          <Head>
            <HeadLeft>
              <h2>{this.state.name}
                <span>{this.state.id ? `ID: ${this.state.id}` : "ID:"}</span>
              </h2>
            </HeadLeft>
            <HeadRight>
              <button
                type="button"
                className="btn btnDelete"
                onClick={() => this.setState({ modalClique: true })}
              >
                Delete
              </button>
            </HeadRight>
          </Head>
          <Padd padd={30}>
            <Row>
              <Col>
                <form onSubmit={this.handleSubmit}>
                  <div 
                    className="form-group row"
                  >
                    <label
                      htmlFor="name"
                      className="col-4 col-form-label">
                      Name:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="name"
                        required="true"
                        className="form-control" 
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.name}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label
                      htmlFor="hometown"
                      className="col-4 col-form-label">
                      Hometown:
                    </label>
                    <div
                      className="col-8"
                    >
                      <input
                        id="hometown"
                        ref="hometown"
                        required="true"
                        placeholder=""
                        className="form-control"
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.hometown}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <label
                      htmlFor="creator"
                      className="col-4 col-form-label">
                      Creator:
                    </label>
                    <div
                      className="col-8"
                    >
                      <Autocomplete
                        id="creatorQuery"
                        required={true}
                        type="user"
                        value={this.state.creatorQuery}
                        onChange={this.handleChange}
                        loading={this.state.isCreatorSearchLoading}
                        data={this.state.creatorResult}
                        handleSelect={this.handleSelectCreator}
                      />
                    </div>
                  </div>
                  <div
                    className="form-group row"
                  >
                    <div
                      className="col-8 offset-4"
                    >
                      <LoadingButton
                        type="submit"
                        loading={this.state.isCliqueLoading}>
                        Save
                      </LoadingButton>
                    </div>
                  </div>
                </form>
              </Col>
              <Col>
                <div
                  className="avatarUpload"
                >
                  <Avatar 
                    size={100}
                    src={this.state.image_url}
                    default={Svg.AvatarDefaultClique}
                  />
                  <LoadingButton
                    type="button"
                    loading={this.state.isPhotoButtonLoading}
                    onClick={this.handleChoosePhoto}
                  >
                    Choose Photo
                  </LoadingButton>
                  <input
                    id="file"
                    ref="upload"
                    type="file"
                    style={{ display: "none" }}
                    onChange={this.handleAvatar}
                  />
                  <button
                    type="button"
                    className="linkRed"
                    onClick={() => this.setState({ modalPhoto: true })}
                  >
                    Remove
                  </button>
                </div>
              </Col>
            </Row>
          </Padd>
          <SubHead>
            <SubHeadLeft>
              <h3>Members:</h3>
            </SubHeadLeft>
          </SubHead>
          <Padd padd={30}>
            <Row>
              <Col col={6}>
                <form>
                  <div
                    className="form-group row"
                  >
                    <div
                      className="col-9"
                    >
                      <Autocomplete
                        id="memberQuery"
                        type="user"
                        value={this.state.memberQuery}
                        placeholder="Add Member"
                        onChange={this.handleChange}
                        loading={this.state.isMemberSearchLoading}
                        data={this.state.memberResult}
                        handleSelect={this.handleSelectMember}
                      />
                    </div>
                    <div
                      className="col-3"
                    >
                      <LoadingButton
                        type="button"
                        loading={this.state.isMemberButtonLoading}
                        onClick={this.handleAddMember}
                      >
                        Add
                      </LoadingButton>
                    </div>
                  </div>
                </form>
              </Col>
            </Row>
          </Padd>
          <TableCliqueEdit
            data={this.state.memberships}
            remove={(e, i) => this.setState({ modalMember: true, member_id_to_remove: i })}
          />
        </ContentLoader>
        <Modal
          visible={this.state.modalPhoto}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_PHOTO}
          cancel={() => this.setState({ modalPhoto: false })}
          yes={this.handleRemovePhoto}
        />
        <Modal
          visible={this.state.modalClique}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_CLIQUE}
          cancel={() => this.setState({ modalClique: false })}
          yes={this.handleDelete}
        />
        <Modal
          visible={this.state.modalMember}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_REMOVE_MEMBER}
          cancel={() => this.setState({ modalMember: false })}
          yes={this.handleRemoveMember}
        />
      </Content>
    )
  }
}

export default CliqueEdit;
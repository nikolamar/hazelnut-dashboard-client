// @flow weak

import React from 'react';

const style = {
  strech: {
    position: "fixed",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  overlay: {
    backgroundColor: "rgba(0,0,0,0.5)",
    animation: "fadeIn 1s",
  },
  modal: {
    maxWidth: 500,
    margin: "auto",
    marginTop: 100,
    animation: "flipInX 0.5s",
  }
};

const Modal = ({ visible, title, body, ...button }) => visible ? (
  <div style={style.strech}>
    <div 
      style={{
        ...style.strech, 
        ...style.overlay
      }}
    />
    <form
      style={style.modal}
      className="modal-content"
      tabIndex="0"
      onBlur={button.cancel}
      autoFocus
    >
      <div className="modal-header">
        <h5 
          className="modal-title">
          { title }
        </h5>
        <button 
          type="button" 
          className="close" 
          aria-label="Close"
          onClick={button.cancel}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        <p>{ body }</p>
      </div>
      <div className="modal-footer">
        <button 
          type="button" 
          className="btn btn-secondary"
          onClick={button.cancel}
        >
          Cancel
        </button>
        <button 
          type="button" 
          className="btn btn-primary"
          onClick={button.yes}
        >
          Yes
        </button>
      </div>
    </form>
  </div>
) : null;

export default Modal;
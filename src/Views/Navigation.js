// @flow weak

import React from 'react';
import ClassicLink from '../Views/ClassicLink';

const Navigation = ({ children, logo, bottom }) => (
  <div className="navBar">
    { logo }
    <nav>
      <ul>
        { children }
      </ul>
      { bottom }
    </nav>
  </div>
);

Navigation.Link = ClassicLink;

export default Navigation;
// @flow weak

import React, { Component } from 'react';
import AutocompleteTableUser from '../Views/AutocompleteTableUser';
import AutocompleteTableClique from '../Views/AutocompleteTableClique';
import Svg from '../Svg';
import KEYCODE from '../Lib/KEYCODE';

const style = {
  container: {
    position: "relative",
  },
  img: {
    position: "absolute",
    top: 5,
    right: 10,
    opacity: 0.5,
  },
  suggestionContainer: {
    backgroundColor: "#fff",
    position: "absolute",
    left: 0,
    right: 0,
    boxShadow: "0 2px 6px rgba(0,0,0,0.3)",
    overflow: "hidden",
    zIndex: 1000,
  }
}

class Autocomplete extends Component {
  state = {
    open: false,
    selected: 0,
  }
  handleKeyDown = event => {
    switch(event.keyCode) {
      case KEYCODE.ESCAPE_KEY:
        this.setState({ 
          open: false,
          selected: 0,
        });
        break;
      case KEYCODE.UP:
        event.preventDefault();
        this.setState({ 
          selected: (this.state.selected-1) < 0 ?
          0 : this.state.selected-1
        });
        break;
      case KEYCODE.DOWN:
        this.setState({ 
          selected: (this.state.selected+1) >= this.props.data.length || 
          (this.state.selected+1) >= 5 ?
          this.state.selected : this.state.selected+1,
        });
        break;
      case KEYCODE.ENTER:
        event.preventDefault();
        this.props.handleSelect(event, this.state.selected);
        this.setState({ open: false });
        break;
      default: 
        break;
    }
  };
  handleOnChange = event => {
    this.props.onChange(event);
    if (!event.target.value) {
      this.setState({ 
        open: false,
        selected: 0,
      });
    } 
    else {
      this.setState({ 
        open: true,
        selected: this.props.data.length-1 <= this.state.selected ? 
        0 : this.state.selected,
      });
    }
  };
  handleMouse = (event, i) => {
    this.setState({ selected: i });
    if (event.type === "mousedown")
      this.props.handleSelect(event, i);
  };
  collapse = () => {
    this.setState({ open: false });
  };
  render() {
    const loader = this.props.loading ?
    <img
      alt="search"
      style={style.img} 
      src={Svg.LoadingBlack}
      width={20} 
      height={20}
    /> : null;
    const suggestion = this.state.open ?
    <div style={style.suggestionContainer}>
      {
        this.props.type === "user" ? 
        <AutocompleteTableUser
          data={this.props.data}
          selected={this.state.selected}
          handleMouse={this.handleMouse}
        /> :
        <AutocompleteTableClique
          data={this.props.data}
          selected={this.state.selected}
          handleMouse={this.handleMouse}
        />
      }
    </div> : null;
    return (
      <div
        onKeyDown={this.handleKeyDown}
        style={style.container}
        tabIndex="0"
        onBlur={this.collapse}
      >
        <input
          id={this.props.id}
          style={{paddingRight: 30, width: "100%"}}
          autoComplete="off"
          required={this.props.required}
          type="text"
          value={this.props.value}
          placeholder={this.props.placeholder}
          onChange={this.handleOnChange}
        />
        { loader }
        { suggestion }
      </div>
    );
  }
}

export default Autocomplete;
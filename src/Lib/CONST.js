// @flow weak

class Const {
  static API_KEY = "hazelnut_admin";
  static API_SECRET = "hazelnut_12345";
  static BASE_URL = "https://murmuring-bastion-26624.herokuapp.com/";
  static GOOGLE_API_KEY = "AIzaSyAkmxLZHVpHObPVDKYVjz73HciL7b97j5k";
  static CLOUDINARY_CLOUD_NAME = "dporul9yg";
  static CLOUDINARY_PRESET_NAME = "hazelnut";
  static MESSAGE = {
    CONNECTION: "Please check your internet connection!",
    ADDED: "Well done! You've added ",
    UPDATED: "Well done! You've updated ",
    DELETED: "Well done! You've deleted ",
    PHOTO_REMOVED: "Well done! You've removed photo from ",
    MEMBER_ADDED: "Well done! You've added member to ",
    MEMBER_REMOVED: "Well done! You've removed member from ",
    MEMBER_DOES_NOT_EXIST: "Member doesn't exist!",
    MAKE_OWNER: "You have changed user to owner!",
    MAKE_WINNER: "You have changed user to winner!",
    WARNING_TITLE: "Warning",
    WARNING_DELETE_PHOTO: "Are you sure you want to remove photo?",
    WARNING_DELETE_USER: "Are you sure you want to remove user?",
    WARNING_DELETE_CLIQUE: "Are you sure you want to remove clique?",
    WARNING_DELETE_CHALLENGE: "Are you sure you want to remove challenge?",
    WARNING_REMOVE_MEMBER: "Are you sure you want to remove member from clique?",
    LOADING_MORE: "Loading more..",
    SEARCH: "Please use search box to filter results, type min. 3 characters.",
    NO_RESULTS: "There are no results that match your search",
  };
  static TYPE = {
    ERROR: "ERROR",
    SUCCESS: "SUCCESS",
    INFO: "INFO",
  };
  static DISCIPLINE = {
    "1": "Sport",
    "2": "Fun",
    "3": "Home",
    "4": "Other",
  };
}

export default Const;
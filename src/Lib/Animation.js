// @flow weak

import RequestAnimationFrame from '../Lib/RequestAnimationFrame';

class Animation {
  timerID;
  start(func, easing = "easeOutSine", time = 0.3) {
    this.timerID = null;
    let currentTime = 0;
    /* eslint-disable */
    const easingEquations = {
      easeOutSine: pos => Math.sin(pos * (Math.PI / 2)),
      easeInOutSine: pos => (-0.5 * (Math.cos(Math.PI * pos) - 1)),
      easeInOutQuint: pos => ((pos /= 0.5) < 1) ? 0.5 * Math.pow(pos, 5) : 0.5 * (Math.pow((pos - 2), 5) + 2), 
    };
    /* eslint-enable */
    const tick = () => {
      currentTime += 1 / 60;
      const p = currentTime / time;
      const t = easingEquations[easing](p);
      if (p < 1) {
        this.timerID = RequestAnimationFrame(tick);
        func(t);
      } else {
        // animation ends
      }
    }
    this.timerID = requestAnimationFrame(tick);
  };
  cancel() {
    if (cancelAnimationFrame)
      cancelAnimationFrame(this.timerID);
    else
      clearTimeout(this.timerID);
  };
}

export default Animation;
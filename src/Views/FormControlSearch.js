// @flow weak

import React from 'react';
import Svg from '../Svg';

const style = {
  container: {
    position: "relative",
  },
  img: {
    position: "absolute",
    top: 5,
    right: 10,
    opacity: 0.5,
  }
}

const FormControlSearch = ({ id, type, placeholder, value, onChange, loading }) => (
  <div style={style.container}>
    <div className="form-group">
      <input
        style={{paddingRight: 30}}
        id={id}
        type={type}
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        autoComplete="off"
      />
    </div>
    {
      loading ?
      <img
        alt="search"
        style={style.img} 
        src={Svg.LoadingBlack}
        width={20} 
        height={20}
      /> : null
    }
  </div>
);

export default FormControlSearch;
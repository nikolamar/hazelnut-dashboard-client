// @flow weak

import React, { Component } from 'react';
import moment from 'moment';
import DateTime from 'react-datetime';
import Content, {
  Head, 
  HeadLeft, 
  HeadRight,
  SubHead,
  SubHeadLeft,
  Padd,
  Row,
  Col, 
} from '../Views/Content';
import Modal from '../Views/Modal';
import { toast } from '../Views/Toast';
import LoadingButton from '../Views/LoadingButton';
import ContentLoader from '../Views/ContentLoader';
import Autocomplete from '../Views/Autocomplete';
import Avatar from '../Views/Avatar';
import Api from '../Lib/Api';
import CONST from '../Lib/CONST';
import Svg from '../Svg';
import TableChallengesEdit from '../Views/TableChallengesEdit';
import KEYCODE from '../Lib/KEYCODE';

class ChallengesEdit extends Component {
  state = {
    modalPhoto: false,
    modalChallenge: false,
    modalMember: false,
    isPageLoading: true,
    isPhotoButtonLoading: false,
    isCliqueSearchLoading: false,
    isMemberButtonLoading: false,
    isMemberSearchLoading: false,
    isChallengesLoading: false,
    member_id_to_remove: null,
    clique: {},
    cliqueQuery: "",
    cliqueResult: [],
    member: {},
    memberQuery: "",
    memberResult: [],
    id: null,
    discipline: "",
    description: "",
    betted_hazelnuts: 0,
    discipline_category_id: 1,
    start_time: "",
    dateTimePicker: "",
    image_url: "",
    place_id: "",
    clique_id: null,
    locationPlace: "",
    longitude: "",
    latitude: "",
    challenge_users: [],
  }
  locationPlaceSearch = {};
  keydownListener = {};
  locationPlaceChangedListener = {};
  handleDateTimeChange = event => {
    this.setState({
      dateTimePicker: event,
      start_time: event.format("YYYY-MM-DDThh:mm:ss.SSSZ"),
    });
  }
  handleChange = event => {
    switch(event.target.id) {
      case "cliqueQuery":
        if (!event.target.value) {
          this.setState({
            cliqueResult: [],
            isCliqueSearchLoading: false,
            [event.target.id]: event.target.value,
          });
          return;
        }
        this.setState({
          isCliqueSearchLoading: true,
          [event.target.id]: event.target.value,
        });
        Api
          .cancel()
          .searchCliques(event.target.value)
          .then(({ data }) => {
            this.setState({ 
              isCliqueSearchLoading: false,
              cliqueResult: data 
            });
          })
          .catch(error => {
            if (Api.axios.isCancel(error)) {} else {
              this.setState({ isCliqueSearchLoading: false });
              toast((error.response && error.response.data.message) 
              || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
            }
          });
        break;
      case "memberQuery":
        if (!event.target.value) {
          this.setState({
            memberResult: [],
            isMemberSearchLoading: false,
            [event.target.id]: event.target.value,
          });
          return;
        }
        this.setState({
          isMemberSearchLoading: true,
          [event.target.id]: event.target.value,
        });
        Api
          .cancel()
          .searchUsers(event.target.value)
          .then(({ data }) => {
            this.setState({ 
              isMemberSearchLoading: false,
              memberResult: data 
            });
          })
          .catch(error => {
            if (Api.axios.isCancel(error)) {} else {
              this.setState({ isMemberSearchLoading: false });
              toast((error.response && error.response.data.message) 
              || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
            }
          });
        break;
      default:
        this.setState({ [event.target.id]: event.target.value });
        break;
    }
  };
  handleChoosePhoto = () => {
    this.refs.upload.click();
  };
  handleAvatar = event => {
    const file = event.target.files[0];
    this.setState({ isPhotoButtonLoading: true });
    Api
      .cloudinaryUpload(file)
      .then(({ data }) => {
        this.setState({
          image_url: data.secure_url,
          isPhotoButtonLoading: false,
        });
        this.refs.upload.value = null;
      })
      .catch(error => {
        this.refs.upload.value = null;
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isChallengesLoading: true });
    let data = {};
    if (this.state.place_id) data.place_id = this.state.place_id;
    data.discipline = this.state.discipline;
    data.discipline_category_id = this.state.discipline_category_id;
    data.image_url = this.state.image_url;
    data.description = this.state.description;
    data.betted_hazelnuts = this.state.betted_hazelnuts;
    data.start_date = this.state.start_time;
    data.clique_id = this.state.clique_id;
    (this.state.id ? Api.updateChallenge(data, this.state.id) : Api.createChallenge(data, this.state.id))
      .then(({ data }) => {
        this.state.id ?
        toast(`${CONST.MESSAGE.UPDATED} ${data.discipline}!`, CONST.TYPE.SUCCESS) :
        toast(`${CONST.MESSAGE.ADDED} ${data.discipline}!`, CONST.TYPE.SUCCESS);
        this.setState({
          isChallengesLoading: false,
          ...data,
        });
        this.props.history.push(`/challenges/${data.id}`);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isChallengesLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleSelectClique = (event, i) => {
    this.setState({
      clique: this.state.cliqueResult[i],
      clique_id: this.state.cliqueResult[i].id,
      cliqueQuery: this.state.cliqueResult[i].name
    });
  };
  handleSelectUser = (event, i) => {
    this.setState({
      member: this.state.memberResult[i],
      memberQuery: this.state.memberResult[i].nickname
    });
  };
  handleAddMember = event => {
    this.setState({ isMemberButtonLoading: true });
    let data = this.state.challenge_users.slice();
    if (this.state.memberQuery !== this.state.member.nickname)
      data.push({ 
        user: this.state.memberResult.filter(member => member.nickname === this.state.memberQuery)[0] 
      });
    else
      data.push({ 
        user: this.state.member 
      });
    if (!data[data.length-1].user) {
      toast(CONST.MESSAGE.MEMBER_DOES_NOT_EXIST, CONST.TYPE.ERROR);
      return;
    }
    Api
      .addUserToChallenge(
        this.state.id, 
        data[data.length-1].user.id,
      )
      .then(() => {
        this.setState({ 
          isMemberButtonLoading: false,
          challenge_users: data,
        });
        toast(`${CONST.MESSAGE.MEMBER_ADDED} ${data[data.length-1].user.nickname}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isMemberButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleRemoveMember = () =>  {
    this.setState({ 
      isMemberButtonLoading: true,
      modalMember: false,
    });
    Api
      .removeUserFromChallenge(this.state.id, this.state.member_id_to_remove)
      .then(() => {
        this.setState({ 
          isMemberButtonLoading: false,
          challenge_users: this.state.challenge_users.filter(
            ({ user }) => user.id !== this.state.member_id_to_remove),
        });
        toast(`${CONST.MESSAGE.MEMBER_REMOVED} ${this.state.discipline}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isMemberButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  }
  handleRemovePhoto = () => {
    this.setState({ 
      isPhotoButtonLoading: true,
      modalPhoto: false,
    });
    Api
      .updateChallenge({ image_url: "" }, this.state.id)
      .then(({ data }) => {
        toast(`${CONST.MESSAGE.PHOTO_REMOVED} ${data.discipline}!`, CONST.TYPE.SUCCESS);
        this.setState({
          isPhotoButtonLoading: false,
          image_url: "",
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isPhotoButtonLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      }); 
  };
  handleDelete = () => {
    this.setState({ 
      isChallengesLoading: true,
      modalClique: false,
    });
    Api
      .deleteChallenge(this.state.id)
      .then(() => {
        this.props.history.push("/challenges");
        toast(`${CONST.MESSAGE.DELETED} ${this.state.discipline}!`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isChallengesLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  makeWinner = (event, userId) => {
    this.setState({ isChallengesLoading: true });
    Api
      .updateWinnerForChallenge(this.state.id, userId)
      .then(() => {
        this.setState({ isChallengesLoading: false });
        toast(CONST.MESSAGE.MAKE_WINNER, CONST.TYPE.SUCCESS);
        this.refresh();
      })
      .catch(error => {
        this.setState({ isChallengesLoading: false });
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isChallengesLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  }
  makeOwner = (event, userId) => {
    this.setState({ isChallengesLoading: true });
    Api
      .updateOwnerForChallenge(this.state.id, userId)
      .then(() => {
        this.setState({ isChallengesLoading: false });
        toast(CONST.MESSAGE.MAKE_OWNER, CONST.TYPE.SUCCESS);
        this.refresh();
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ isChallengesLoading: false });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  refresh = () => {
    Api
      .getChallenge(this.props.match.params.id)
      .then(({ data }) => {
        this.setState({
          ...data, 
          start_time: moment(data.start_date).format("YYYY-MM-DDThh:mm:ss.SSSZ"),
          dateTimePicker: moment(data.start_date),
          locationPlace: data.location ? data.location.name : "",
          longitude: data.location ? data.location.longitude : "",
          latitude: data.location ? data.location.latitude : "",
        });
        return Api.getCliqueDetails(data.clique.id);
      })
      .then(({ data }) => {
        this.setState({
          isPageLoading: false,
          isChallengesLoading: false,
          cliqueQuery: data.name,
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({
            isPageLoading: false,
            isChallengesLoading: false,
          });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  componentDidMount() {
    if (!window.google) return;
    this.locationPlaceSearch = new window.google.maps.places.Autocomplete(this.refs.locationPlace);
    this.keydownListener = window.google.maps.event.addDomListener(this.refs.locationPlace, "keydown", event => { 
      if (event.keyCode === KEYCODE.ENTER) {
        event.preventDefault(); 
      }
    });
    this.locationPlaceChangedListener = this.locationPlaceSearch.addListener("place_changed", () => {
      const place = this.locationPlaceSearch.getPlace();
      this.setState({
        place_id: place.place_id,
        locationPlace: place.name,
        longitude: place.geometry.location.lng(),
        latitude: place.geometry.location.lat(),
      });
    });
  }
  componentWillMount = () => {
    if (isNaN(this.props.match.params.id)) {
      this.setState({ isPageLoading: false });
      return;
    }
    this.refresh();
  };
  componentWillUnmount() {
    window.google.maps.event.clearInstanceListeners(this.keydownListener);
    window.google.maps.event.clearInstanceListeners(this.locationPlaceChangedListener);
    this.locationPlaceSearch = null;
    Api.cancel();
  }
  render() {
    return (
      <Content>
        <ContentLoader 
          animation="fadeIn" 
          duration={0.5}
          loading={this.state.isPageLoading}
        >
          <Head>
            <HeadLeft>
              <h2>{this.state.discipline}
                <span>{this.state.id ? `ID: ${this.state.id}` : "ID:"}</span>
              </h2>
            </HeadLeft>
            <HeadRight>
              <button
                type="button"
                className="btn btnDelete"
                onClick={() => this.setState({ modalMember: true })}
              >
                Delete
              </button>
            </HeadRight>
          </Head>
          <Padd 
            padd={30}
          >
            <Row>
              <Col>
                <form onSubmit={this.handleSubmit}>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="name" 
                      className="col-4 col-form-label">
                      Discipline:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="discipline"
                        className="form-control" 
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.discipline}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="description" 
                      className="col-4 col-form-label">
                      Description:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <textarea 
                        className="form-control" 
                        id="description"
                        onChange={this.handleChange}
                        value={this.state.description}
                      >
                      </textarea>
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="discipline_category_id" 
                      className="col-4 col-form-label">
                      Category:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <select
                        id="discipline_category_id"
                        className="form-control" 
                        defaultValue={this.state.discipline_category_id}
                        onChange={this.handleChange}
                      >
                        <option value="1">Other</option>
                        <option value="2">Fun</option>
                        <option value="3">Home</option>
                        <option value="4">Sport</option>
                      </select>
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="betted_hazelnuts" 
                      className="col-4 col-form-label">
                      Betted Hazelnuts:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="betted_hazelnuts"
                        className="form-control" 
                        type="number"
                        onChange={this.handleChange}
                        value={this.state.betted_hazelnuts}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="locationPlace" 
                      className="col-4 col-form-label">
                      Location:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="locationPlace"
                        ref="locationPlace"
                        required="true"
                        placeholder=""
                        className="form-control" 
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.locationPlace}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="latitude" 
                      className="col-4 col-form-label">
                      Latitude:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="latitude"
                        className="form-control" 
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.latitude}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="longitute" 
                      className="col-4 col-form-label">
                      Longitute:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <input
                        id="longitute"
                        className="form-control" 
                        type="text"
                        onChange={this.handleChange}
                        value={this.state.longitude}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="startTime" 
                      className="col-4 col-form-label">
                      Start Time:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <DateTime
                        defaultValue={this.state.dateTimePicker}
                        value={this.state.dateTimePicker}
                        onChange={this.handleDateTimeChange}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <label 
                      htmlFor="clique" 
                      className="col-4 col-form-label">
                      Clique:
                    </label>
                    <div 
                      className="col-8"
                    >
                      <Autocomplete
                        id="cliqueQuery"
                        required={true}
                        type="clique"
                        value={this.state.cliqueQuery}
                        onChange={this.handleChange}
                        loading={this.state.isCliqueSearchLoading}
                        data={this.state.cliqueResult}
                        handleSelect={this.handleSelectClique}
                      />
                    </div>
                  </div>
                  <div 
                    className="form-group row"
                  >
                    <div 
                      className="col-8 offset-4"
                    >
                      <LoadingButton
                        type="submit"
                        loading={this.state.isChallengesLoading}>
                        Save
                      </LoadingButton>
                    </div>
                  </div>
                </form>
              </Col>
              <Col>
              <div 
                className="avatarUpload"
              >
                <Avatar 
                  size={100}
                  src={this.state.image_url}
                  default={Svg.AvatarDefaultChallenge}
                />
                <LoadingButton
                  type="button"
                  loading={this.state.isPhotoButtonLoading}
                  onClick={this.handleChoosePhoto}
                >
                  Choose Photo
                </LoadingButton>
                <input
                  id="file"
                  ref="upload"
                  type="file"
                  style={{ display: "none" }}
                  onChange={this.handleAvatar}
                />
                <button
                  type="button"
                  className="linkRed"
                  onClick={() => this.setState({ modalPhoto: true })}
                >
                  Remove
                </button>
              </div>
              </Col>
            </Row>
          </Padd>
          <SubHead>
            <SubHeadLeft>
              <h3>Members:</h3>
            </SubHeadLeft>
          </SubHead>
          <Padd padd={30}>
            <Row>
              <Col col={6}>
                <form>
                  <div
                    className="form-group row"
                  >
                    <div
                      className="col-9"
                    >
                      <Autocomplete
                        id="memberQuery"
                        type="user"
                        value={this.state.memberQuery}
                        placeholder="Add User"
                        onChange={this.handleChange}
                        loading={this.state.isMemberButtonLoading}
                        data={this.state.memberResult}
                        handleSelect={this.handleSelectUser}
                      />
                    </div>
                    <div 
                      className="col-3"
                    >
                      <button
                        type="button"
                        className="btn btn-primary"
                        onClick={this.handleAddMember}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </form>
              </Col>
            </Row>
          </Padd>
          <TableChallengesEdit 
            data={this.state.challenge_users}
            remove={(e, i) => this.setState({ modalMember: true, member_id_to_remove: i })}
            makeOwner={this.makeOwner}
            makeWinner={this.makeWinner}
          />
        </ContentLoader>
        <Modal
          visible={this.state.modalPhoto}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_PHOTO}
          cancel={() => this.setState({ modalPhoto: false })}
          yes={this.handleRemovePhoto}
        />
        <Modal
          visible={this.state.modalChallenge}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_DELETE_CHALLENGE}
          cancel={() => this.setState({ modalChallenge: false })}
          yes={this.handleDelete}
        />
        <Modal
          visible={this.state.modalMember}
          title={CONST.MESSAGE.WARNING_TITLE}
          body={CONST.MESSAGE.WARNING_REMOVE_MEMBER}
          cancel={() => this.setState({ modalMember: false })}
          yes={this.handleRemoveMember}
        />
      </Content>
    )
  }
}

export default ChallengesEdit;
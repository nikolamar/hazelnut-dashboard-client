// @flow weak

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { toast } from '../Views/Toast';
import Api from '../Lib/Api';
import CONST from '../Lib/CONST';
import Emitter from '../Lib/Emitter';
import LoadingButton from '../Views/LoadingButton';
import Animate from '../Views/Animate';

class Login extends Component {
  state = {
    username: "",
    password: "",
    loading: false,
  };
  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  };
  handleSubmit = event => {
    const { username, password } = this.state;
    event.preventDefault();
    this.setState({ loading: true });
    Api
      .basicAuthLogin(username, password)
      .then(response => {
        this.setState({ loading: false });
        window.sessionStorage.setItem("hazelnut-token", JSON.stringify(response.data.token));
        Emitter.emit("hazelnut-token", window.sessionStorage.getItem("hazelnut-token"));
        toast(`Welcome ${username}`, CONST.TYPE.SUCCESS);
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ loading: false });
          toast((error.response && error.response.data.message) || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  render() {
    const { loading } = this.state;
    if (this.props.token) {
      return (
        <Redirect to="/users" />
      );
    }
    return (
      <Animate
        className="loginContainer"
        animation="slideInDown" 
        duration={0.5}
      >
        <h2>Login</h2>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label 
              htmlFor="email"
            >
              Email:
            </label>
            <input 
              id="username"
              required="true"
              type="text"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label 
              htmlFor="password"
            >
              Password:
            </label>
            <input 
              id="password"
              required="true"
              type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <LoadingButton
              bsStyle="primary" 
              type="submit"
              loading={loading}
            >
              Login
            </LoadingButton>
          </div>
        </form>
      </Animate>
    );
  }
}

export default Login;
// @flow weak

import React, { Component } from 'react';
import { toast } from '../Views/Toast';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import Content, {
  Head,
  HeadLeft, 
  HeadRight,
  SubHead,
  SubHeadLeft,
  SubHeadRight 
} from '../Views/Content';
import Api from '../Lib/Api';
import CONST from '../Lib/CONST';
import FormControlSearch from '../Views/FormControlSearch';
import Animate from '../Views/Animate';
import TableChallenges from '../Views/TableChallenges';
import LazyLoader from '../Views/LazyLoader';

class Challenges extends Component {
  state = {
    query: "",
    challenges: [],
    isLoading: true,
    order: "asc",
    orderBy: "ID",
    page: 1,
    size: 20,
    pageMessage: "",
    isDoneListing: false,
  };
  search = event => {
    this.setState({ [event.target.id]: event.target.value });
    if (event.target.value.length <= 2) {
      this.setState({ 
        challenges: [],
        pageMessage: CONST.MESSAGE.SEARCH,
        isDoneListing: false,
      });
      if (event.target.value.length === 0) {
        this.setState({
          page: 1,
          pageMessage: "",
        });
        this.fetch(1);
      }
      return;
    }
    this.setState({ isLoading: true });
    Api
      .cancel()
      .searchChallenges(event.target.value)
      .then(({ data }) => {
        const newOrder = _.orderBy(data, this.state.orderBy, this.state.order);
        this.setState({
          isLoading: false,
          challenges: newOrder,
          pageMessage: data.length ? "" : CONST.MESSAGE.NO_RESULTS,
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({ 
            isLoading: false,
            pageMessage: CONST.MESSAGE.CONNECTION,
          });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  handleOrderBy = event => {
    const newOrder = _.orderBy([...this.state.challenges], event.target.value, this.state.order);
    this.setState({ challenges: newOrder, orderBy:  event.target.value });
  };
  handleOrder = event => {
    const newOrder = _.orderBy([...this.state.challenges], this.state.orderBy, event.target.value);
    this.setState({ challenges: newOrder, order: event.target.value });
  };
  handleBottom = () => {
    this.fetch(this.state.page + 1);
    this.setState({ page: this.state.page + 1 });
  };
  fetch = (page = this.state.page) => {
    if (this.state.isDoneListing) {
      return;
    }
    this.setState({ isLoading: true });
    Api
      .cancel()
      .getChallenges(page, this.state.size)
      .then(({ data }) => {
        let newOrder = this.state.challenges.concat(data);
        newOrder = _.orderBy(newOrder, this.state.orderBy, this.state.order);
        this.setState({
          isLoading: false,
          challenges: newOrder,
          isDoneListing: data.length && data.length === this.state.size ? false : true,
        });
      })
      .catch(error => {
        if (Api.axios.isCancel(error)) {} else {
          this.setState({
            isLoading: false,
            pageMessage: CONST.MESSAGE.CONNECTION,
          });
          toast((error.response && error.response.data.message) 
          || CONST.MESSAGE.CONNECTION, CONST.TYPE.ERROR);
        }
      });
  };
  componentWillMount() {
    this.fetch();
  }
  componentWillUnmount() {
    Api.cancel();
  }
  render() {
    return (
      <Content>
        <Animate 
          animation="fadeIn" 
          duration={0.5}
        >
          <LazyLoader
            fontSize={12}
            loaderSize={45}
            loading={this.state.isLoading}
            onBottom={this.handleBottom}
            loadingMessage={CONST.MESSAGE.LOADING_MORE}
          >
            <Head>
              <HeadLeft>
                <h2>Challenges</h2>
              </HeadLeft>
              <HeadRight>
                <Link to="/challenges/add">
                  <button
                    type="button" 
                    className="btn btn-primary"
                  >
                    Add Challenge
                  </button>
                </Link>
              </HeadRight>
            </Head>
            <SubHead>
              <SubHeadLeft>
                <FormControlSearch
                  id="query"
                  type="text"
                  value={this.state.query}
                  placeholder="Search Challenges"
                  onChange={this.search}
                  loading={this.state.isLoading}
                />
              </SubHeadLeft>
              <SubHeadRight>
                <div className="form-inline">
                  <select
                    className="form-control" 
                    defaultValue={this.state.orderBy}
                    onChange={this.handleOrderBy}
                  >
                    <option value="id">ID</option>
                    <option value="discipline">Discipline</option>
                    <option value="discipline_category_id">Category</option>
                    <option value="start_date">Start Date</option>
                  </select>
                  <select
                    style={{ marginLeft: 20 }}
                    className="form-control" 
                    defaultValue={this.state.order}
                    onChange={this.handleOrder}
                  >
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                  </select>
                </div>
              </SubHeadRight>
            </SubHead>
            { 
              this.state.pageMessage ? 
              <Animate 
                animation="fadeIn" 
                duration={0.5}
                className="text-center"
              >
                {this.state.pageMessage}
              </Animate> :
              <TableChallenges data={this.state.challenges} />
            }
          </LazyLoader>
        </Animate>
      </Content>
    );
  }
}

export default Challenges;
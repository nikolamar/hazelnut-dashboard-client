export default {
  ENTER: 13,
  ESCAPE_KEY: 27,
  UP: 38,
  DOWN: 40,
}
// @flow weak

import React from 'react';
import Svg from '../Svg';
import Avatar from './Avatar';

const style = {
  container: {
    overflow: "hidden",
  },
  table: { 
    margin: 0,
  },
  column: {
    minWidth: 60,
  },
  item: {
    cursor: "pointer",
  },
  itemSelected: {
    cursor: "pointer",
    backgroundColor: "#eeeeee",
    transition: "background .15s linear",
  }
};

const body = (data, handleMouse, selected) => [...data.slice(0, 5)].map((el, i) => (
  <tr
    key={i}
    style={selected === i ? style.itemSelected : style.item}
    onMouseDown={event => handleMouse(event, i)}
    onMouseOver={event => handleMouse(event, i)}
  >
    <td
      style={style.column}
    >
      <div className="user">
        <Avatar
          style={{marginRight: 10}}
          size={30}
          src={el.image_url}
          default={Svg.AvatarDefaultUser}
        />
        <div className="name">
        { `${el.first_name} ${el.last_name} ` }<span>{`(${el.nickname})`}</span>
        </div>
      </div>
    </td>
  </tr>
));

const AutocompleteTableUser = ({ data, icon, handleMouse, selected }) => (
  <div style={style.container}>
    <table style={style.table}>
      <tbody>
        { body(data, handleMouse, selected) }
      </tbody>
    </table>
  </div>
);

export default AutocompleteTableUser;
// @flow weak

import React from 'react';

const NoMatch = ({ location }) => (
  <div className="mainContainer">
    <div className="whiteContainer">
      <div className="pageHead">
        <h2>No match for <code>{location.pathname}</code> change your url in address bar</h2>
      </div>
    </div>
  </div>
);

export default NoMatch;